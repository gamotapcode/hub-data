const Koa = require('koa');
const app = new Koa();

app.use(async ctx => {
    console.log('header: ', ctx.header)
    console.log('---------')
    console.log('body ', ctx.body)
});

app.listen(8001);